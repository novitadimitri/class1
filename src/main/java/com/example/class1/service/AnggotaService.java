package com.example.class1.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.class1.model.AnggotaModel;
import com.example.class1.repository.AnggotaRepository;


@Service
@Transactional
public class AnggotaService {

		@Autowired //untuk memanggil fakultas repository dan untuk menamai nya menjadi nama baru yang sama(huruf kecil)
		private AnggotaRepository anggotaRepository;
		
		public void save (AnggotaModel anggotaModel) { //untuk fungsi save
			anggotaRepository.save(anggotaModel);
		}
		
		public List<AnggotaModel> read() {
			return this.anggotaRepository.findAll(); //untuk list semua yg ada di database
		}
		
		public AnggotaModel searchKodeAnggota(String kodeAnggota) { 
			return this.anggotaRepository.searchKodeAnggota(kodeAnggota);
		}
		public void update (AnggotaModel anggotaModel) { //untuk fungsi update
			anggotaRepository.save(anggotaModel);
		}
		public void delete (AnggotaModel anggotaModel) { //untuk fungsi delete
			anggotaRepository.delete(anggotaModel);
		}
		public List<AnggotaModel> searchNamaAnggota(String namaAnggota) { //untuk fungsi search
			return this.anggotaRepository.searchNamaAnggota(namaAnggota);
		}
}