package com.example.class1.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.class1.model.FakultasModel;
import com.example.class1.service.FakultasService;

@Controller /* Untuk merequest sebuah url */ 
@RequestMapping ("/fakultas")
public class FakultasController {
	
	@Autowired
	private FakultasService fakultasService;
	
	@RequestMapping("/home")
	public String fakultas()
	{
		return "/fakultas/home";
	}
	
	@RequestMapping("/add")
	public String doAdd()
	{
		return "/fakultas/add";
	}
	
	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request)
	{
		String kodeFakultas = request.getParameter("kodeFakultas");
		String namaFakultas = request.getParameter("namaFakultas");
		
		FakultasModel fakultasModel = new FakultasModel();// instance
		fakultasModel.setKodeFakultas(kodeFakultas);
		fakultasModel.setNamaFakultas(namaFakultas);
		
		this.fakultasService.save(fakultasModel);
		
		return "/fakultas/home";
	}
	
	@RequestMapping("/data")
	public String doList(Model model)
	{
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		fakultasModelList = this.fakultasService.read();
		model.addAttribute("fakultasModelList", fakultasModelList);
		
		return "/fakultas/list";
	}
	
	@RequestMapping("/detail")
	public String doDetail(HttpServletRequest request, Model model)
	{
		String kodeFakultas = request.getParameter("kodeFakultas");
		FakultasModel fakultasModel = new FakultasModel();
		fakultasModel = this.fakultasService.searchKodeFakultas(kodeFakultas);
		model.addAttribute("fakultasModel", fakultasModel);
		
		return "/fakultas/detail";
	}
	
	@RequestMapping("/ubah") // request pop-up
	public String doEdit(HttpServletRequest request, Model model)
	{
		String kodeFakultas = request.getParameter("kodeFakultas");
		FakultasModel fakultasModel = new FakultasModel();
		fakultasModel = this.fakultasService.searchKodeFakultas(kodeFakultas);
		model.addAttribute("fakultasModel", fakultasModel);
		
		return "/fakultas/edit";
	}
	
	@RequestMapping("/update")
	public String doUpdate(HttpServletRequest request)
	{
		String kodeFakultas = request.getParameter("kodeFakultas");
		String namaFakultas = request.getParameter("namaFakultas");
		
		FakultasModel fakultasModel = new FakultasModel();// instance
		fakultasModel.setKodeFakultas(kodeFakultas);
		fakultasModel.setNamaFakultas(namaFakultas);
		
		this.fakultasService.update(fakultasModel);
		
		return "/fakultas/home";
	}
	
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model)
	{
		String kodeFakultas = request.getParameter("kodeFakultas");
		FakultasModel fakultasModel = new FakultasModel();
		fakultasModel = this.fakultasService.searchKodeFakultas(kodeFakultas);
		
		this.fakultasService.delete(fakultasModel);
		
		return "/fakultas/home";
	}
	
	@RequestMapping("/search")
	public String doSearchNama(HttpServletRequest request, Model model) {
		String namaFakultas = request.getParameter("namaFakultas");
		
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		fakultasModelList = this.fakultasService.searchNamaFakultas(namaFakultas);
		model.addAttribute("fakultasModelList", fakultasModelList);
		
		return "/fakultas/search";
	}
	
	@RequestMapping("/cari/kode")
	public String cariKode(HttpServletRequest request, Model model) /* Requst untuk merequest dari back end ke front end */
	{ 
		String kodeController = request.getParameter("kodeHTML");
		System.out.println(kodeController);
		List<FakultasModel> fakultasModelList = this.fakultasService.cariKodeFakultasDonk(kodeController);
		model.addAttribute("fakultasModelList", fakultasModelList);
		return "/fakultas/home";
	}
	
}
